![Build Status](https://gitlab.com/pages/hugo/badges/master/build.svg)

---

Paulo Henrique Correia Website

Strategies and tools for an up-to-date organization.

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [GitLab CI](#gitlab-ci)
- [Building locally](#building-locally)
- [GitLab User or Group Pages](#gitlab-user-or-group-pages)
- [Did you fork this project?](#did-you-fork-this-project)
- [Troubleshooting](#troubleshooting)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## My Projects

__Coming Soon.__

## My workflow

__Coming Soon.__

### Clients

__Coming Soon.__

## Blog

__Coming Soon.__

## Contact

__Coming Soon.__


[ci]: https://about.gitlab.com/gitlab-ci/
[hugo]: https://gohugo.io
[install]: https://gohugo.io/overview/installing/
[documentation]: https://gohugo.io/overview/introduction/
[userpages]: http://doc.gitlab.com/ee/pages/README.html#user-or-group-pages
[projpages]: http://doc.gitlab.com/ee/pages/README.html#project-pages
[post]: https://about.gitlab.com/2016/04/07/gitlab-pages-setup/#custom-domains

<!-- This page was created following the instruction on -->
<!-- https://gitlab.com/help/user/project/pages/index.md -->
<!-- And https://www.youtube.com/watch?v=TWqh9MtT4Bg -->
